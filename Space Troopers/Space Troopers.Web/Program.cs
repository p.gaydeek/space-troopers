using Microsoft.AspNetCore.Authentication.Negotiate;
using Microsoft.EntityFrameworkCore;
using Microsoft.OpenApi.Models;

using SpaceTroopers.Repository;
using SpaceTroopers.Repository.Core;
using SpaceTroopers.Repository.Core.IConfiguration;
using SpaceTroopers.Repository.Core.IRepositories;
using SpaceTroopers.Repository.Models;
using Microsoft.AspNetCore.Identity;

var builder = WebApplication.CreateBuilder(args);

// Services

#region Configure Swagger 
builder.Services.AddSwaggerGen(c =>
{
    c.SwaggerDoc("developper", new OpenApiInfo
    {
        Title = "SpaceTroopersAPI",
        Version = "developper"
    });
});
#endregion

#region Configure DbContext 
builder.Services.AddDbContext<SpaceTroopersContext>(options =>
    options.UseSqlServer(builder.Configuration.GetConnectionString("DefaultConnection")));
#endregion

#region Configure Default Identity
builder.Services.AddDefaultIdentity<Trooper>(options =>
{
    options.SignIn.RequireConfirmedAccount = false;
    options.SignIn.RequireConfirmedPhoneNumber = false;
    options.SignIn.RequireConfirmedEmail = false;
    options.User.RequireUniqueEmail = true;
}).AddRoles<Role>().AddEntityFrameworkStores<SpaceTroopersContext>();
#endregion

#region Configure Controllers
builder.Services.AddControllersWithViews().AddNewtonsoftJson(options =>
        options.SerializerSettings.ReferenceLoopHandling = Newtonsoft.Json.ReferenceLoopHandling.Ignore);
#endregion

#region Authentication Scheme
builder.Services.AddAuthentication(NegotiateDefaults.AuthenticationScheme).AddNegotiate();
#endregion

#region AutoMapper
builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
#endregion

builder.Services.AddScoped(typeof(IUnitOfWork<>), typeof(UnitOFWork<>));
builder.Services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));


builder.Services.AddAuthorization(options =>
{
    // By default, all incoming requests will be authorized according to the default policy.
    options.FallbackPolicy = options.DefaultPolicy;
});
builder.Services.AddRazorPages();


// Runtime App
var app = builder.Build();

// Configure the HTTP request pipeline.
if (!app.Environment.IsDevelopment())
{
    app.UseExceptionHandler("/Home/Error");
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();
}
else
{
    app.UseSwagger();
    app.UseSwaggerUI(c =>
    {
        c.SwaggerEndpoint("/swagger/developper/swagger.json", "SpaceTroopersAPI");
    });
    app.UseDeveloperExceptionPage();
    app.UseMigrationsEndPoint();
}

app.UseHttpsRedirection();
app.UseStaticFiles();

app.UseRouting();

app.UseAuthentication();
app.UseAuthorization();

app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");
app.MapRazorPages();

app.Run();
