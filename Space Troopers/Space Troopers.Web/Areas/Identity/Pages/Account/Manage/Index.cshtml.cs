﻿#nullable disable

namespace SpaceTroopers.Web.Areas.Identity.Pages.Account.Manage
{
    using System.ComponentModel.DataAnnotations;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using SpaceTroopers.Repository.Models;

    public class IndexModel : PageModel
    {
        private readonly UserManager<Trooper> _userManager;
        private readonly SignInManager<Trooper> _signInManager;

        public IndexModel(
            UserManager<Trooper> userManager,
            SignInManager<Trooper> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }

        public string Username { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Phone]
            [Display(Name = "Phone number")]
            public string PhoneNumber { get; set; }
        }

        private async Task LoadAsync(Trooper trooper)
        {
            var userName = await _userManager.GetUserNameAsync(trooper);
            var phoneNumber = await _userManager.GetPhoneNumberAsync(trooper);

            Username = userName;

            Input = new InputModel
            {
                PhoneNumber = phoneNumber
            };
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var trooper = await _userManager.GetUserAsync(User);
            if (trooper == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(trooper);
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            var trooper = await _userManager.GetUserAsync(User);
            if (trooper == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(trooper);
                return Page();
            }

            var phoneNumber = await _userManager.GetPhoneNumberAsync(trooper);
            if (Input.PhoneNumber != phoneNumber)
            {
                var setPhoneResult = await _userManager.SetPhoneNumberAsync(trooper, Input.PhoneNumber);
                if (!setPhoneResult.Succeeded)
                {
                    StatusMessage = "Unexpected error when trying to set phone number.";
                    return RedirectToPage();
                }
            }

            await _signInManager.RefreshSignInAsync(trooper);
            StatusMessage = "Your profile has been updated";
            return RedirectToPage();
        }
    }
}
