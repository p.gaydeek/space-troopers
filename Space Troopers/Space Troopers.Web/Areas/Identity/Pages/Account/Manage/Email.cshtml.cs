﻿#nullable disable

namespace SpaceTroopers.Web.Areas.Identity.Pages.Account.Manage
{
    using System.ComponentModel.DataAnnotations;
    using System.Text;
    using System.Text.Encodings.Web;

    using Microsoft.AspNetCore.Identity;
    using Microsoft.AspNetCore.Identity.UI.Services;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    using Microsoft.AspNetCore.WebUtilities;
    using SpaceTroopers.Repository.Models;

    public class EmailModel : PageModel
    {
        private readonly UserManager<Trooper> _userManager;
        private readonly SignInManager<Trooper> _signInManager;
        private readonly IEmailSender _emailSender;

        public EmailModel(
            UserManager<Trooper> userManager,
            SignInManager<Trooper> signInManager,
            IEmailSender emailSender)
        {
            _userManager = userManager;
            _signInManager = signInManager;
            _emailSender = emailSender;
        }

        public string Email { get; set; }

        public bool IsEmailConfirmed { get; set; }

        [TempData]
        public string StatusMessage { get; set; }

        [BindProperty]
        public InputModel Input { get; set; }

        public class InputModel
        {
            [Required]
            [EmailAddress]
            [Display(Name = "New email")]
            public string NewEmail { get; set; }
        }

        private async Task LoadAsync(Trooper trooper)
        {
            var email = await _userManager.GetEmailAsync(trooper);
            Email = email;

            Input = new InputModel
            {
                NewEmail = email,
            };

            IsEmailConfirmed = await _userManager.IsEmailConfirmedAsync(trooper);
        }

        public async Task<IActionResult> OnGetAsync()
        {
            var trooper = await _userManager.GetUserAsync(User);
            if (trooper == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            await LoadAsync(trooper);
            return Page();
        }

        public async Task<IActionResult> OnPostChangeEmailAsync()
        {
            var trooper = await _userManager.GetUserAsync(User);
            if (trooper == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(trooper);
                return Page();
            }

            var email = await _userManager.GetEmailAsync(trooper);
            if (Input.NewEmail != email)
            {
                var trooperId = await _userManager.GetUserIdAsync(trooper);
                var code = await _userManager.GenerateChangeEmailTokenAsync(trooper, Input.NewEmail);
                code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
                var callbackUrl = Url.Page(
                    "/Account/ConfirmEmailChange",
                    pageHandler: null,
                    values: new { area = "Identity", userId = trooperId, email = Input.NewEmail, code = code },
                    protocol: Request.Scheme);
                await _emailSender.SendEmailAsync(
                    Input.NewEmail,
                    "Confirm your email",
                    $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

                StatusMessage = "Confirmation link to change email sent. Please check your email.";
                return RedirectToPage();
            }

            StatusMessage = "Your email is unchanged.";
            return RedirectToPage();
        }

        public async Task<IActionResult> OnPostSendVerificationEmailAsync()
        {
            var trooper = await _userManager.GetUserAsync(User);
            if (trooper == null)
            {
                return NotFound($"Unable to load user with ID '{_userManager.GetUserId(User)}'.");
            }

            if (!ModelState.IsValid)
            {
                await LoadAsync(trooper);
                return Page();
            }

            var trooperId = await _userManager.GetUserIdAsync(trooper);
            var email = await _userManager.GetEmailAsync(trooper);
            var code = await _userManager.GenerateEmailConfirmationTokenAsync(trooper);
            code = WebEncoders.Base64UrlEncode(Encoding.UTF8.GetBytes(code));
            var callbackUrl = Url.Page(
                "/Account/ConfirmEmail",
                pageHandler: null,
                values: new { area = "Identity", userId = trooperId, code = code },
                protocol: Request.Scheme);
            await _emailSender.SendEmailAsync(
                email,
                "Confirm your email",
                $"Please confirm your account by <a href='{HtmlEncoder.Default.Encode(callbackUrl)}'>clicking here</a>.");

            StatusMessage = "Verification email sent. Please check your email.";
            return RedirectToPage();
        }
    }
}
