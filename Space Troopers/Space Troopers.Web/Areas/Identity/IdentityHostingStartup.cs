﻿[assembly: HostingStartup(typeof(SpaceTroopers.Web.Areas.Identity.IdentityHostingStartup))]
namespace SpaceTroopers.Web.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) => {
            });
        }
    }
}