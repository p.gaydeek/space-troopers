﻿namespace Space_Troopers.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    public class MonsterController : Controller
    {
        [Route("monster/all")]
        public IActionResult Monsters()
        {
            return View();
        }

        [Route("monster/single")]
        public IActionResult SingleMonster()
        {
            return View();
        }
    }
}
