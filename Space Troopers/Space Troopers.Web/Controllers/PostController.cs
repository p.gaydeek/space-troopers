﻿namespace Space_Troopers.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    public class PostController : Controller
    {
        [Route("post/all")]
        public IActionResult Posts()
        {
            return View();
        }

        [Route("post/single")]
        public IActionResult SinglePost()
        {
            return View();
        }
    }
}
