﻿namespace SpaceTroopers.Repository.Models
{
    using System.Collections.Generic;

    using SpaceTroopers.Repository.Contracts;
    using SpaceTroopers.Repository.Enums;

    public class Training : BaseEntity, IComentable, ILikeable
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public int Level { get; set; }
        public TypeTrainings Type { get; set; }
        public int TrooperId { get; set; }
        public Trooper Trooper { get; set; }
        public IList<Comment> Comments { get; set; } = new List<Comment>();
        public IList<Like> Likes { get; set; } = new List<Like>();

        public bool IsDeleted { get; set; }
    }
}
