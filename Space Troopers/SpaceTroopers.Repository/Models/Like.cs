﻿namespace SpaceTroopers.Repository.Models
{
    using SpaceTroopers.Repository.Enums;

    public class Like : BaseEntity
    {
        public int TotalLikes { get; set; }
        public Reaction Vote { get; set; }
    }
}
