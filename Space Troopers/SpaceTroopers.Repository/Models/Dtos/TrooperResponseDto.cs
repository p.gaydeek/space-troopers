﻿namespace SpaceTroopers.Repository.Models.Dtos
{
    public class TrooperResponseDto : BaseEntity
    {
        public string? UserName { get; set; }
        public string? Nickname { get; set; }
        public string? Email { get; set; }
    }
}
