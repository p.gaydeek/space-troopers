﻿namespace SpaceTroopers.Repository.Models
{
    using SpaceTroopers.Repository.Contracts;
    public abstract class BaseEntity : IEntity
    {
        public int Id { get; set; }
    }
}
