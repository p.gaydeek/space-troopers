﻿namespace SpaceTroopers.Repository.Models
{
    using Microsoft.AspNetCore.Identity;
    public class Role : IdentityRole<int>
    {
        public Role() : base() { }
        public Role(string roleName) : base(roleName) { }
    }
}
