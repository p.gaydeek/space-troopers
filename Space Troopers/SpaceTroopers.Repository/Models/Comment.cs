﻿namespace SpaceTroopers.Repository.Models
{
    using System.Collections.Generic;

    using SpaceTroopers.Repository.Contracts;

    public class Comment : BaseEntity, ILikeable
    {
        public string? Content { get; set; }
        public int TrooperId { get; set; }
        public Trooper Trooper { get; set; }
        public IList<Like> Likes { get; set; } = new List<Like>();

        public bool IsDeleted { get; set; }
    }
}
