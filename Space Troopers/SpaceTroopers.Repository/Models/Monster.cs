﻿namespace SpaceTroopers.Repository.Models
{
    using SpaceTroopers.Repository.Enums;

    public class Monster : BaseEntity
    {
        public string? Name { get; set; }
        public string? Description { get; set; }
        public TypeMonster Type { get; set; }

        public bool IsDeleted { get; set; }
    }
}
