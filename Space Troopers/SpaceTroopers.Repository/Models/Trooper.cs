﻿namespace SpaceTroopers.Repository.Models
{
    using Microsoft.AspNetCore.Identity;

    using SpaceTroopers.Repository.Contracts;

    public class Trooper : IdentityUser<int>, IEntity
    {
        public string? Nickname { get; set; }

        public bool IsDeleted { get; set; }
    }
}
