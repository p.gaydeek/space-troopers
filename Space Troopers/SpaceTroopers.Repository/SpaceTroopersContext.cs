﻿namespace SpaceTroopers.Repository
{
    using System.Reflection;

    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata;

    using SpaceTroopers.Repository.Models;

    public class SpaceTroopersContext : IdentityDbContext<Trooper, Role, int>
    {
        public SpaceTroopersContext(DbContextOptions<SpaceTroopersContext> options)
            : base(options) { }

        public virtual DbSet<Monster>? Monsters { get; set; }
        public virtual DbSet<Training>? Trainings { get; set; }
        public virtual DbSet<Post>? Posts { get; set; }
        public virtual DbSet<Comment>? Comments { get; set; }
        public virtual DbSet<Like>? Likes { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.ApplyConfigurationsFromAssembly(Assembly.GetExecutingAssembly());

            foreach (IMutableForeignKey foreignKey in modelBuilder.Model.GetEntityTypes().SelectMany(e => e.GetForeignKeys()))
            {
                foreignKey.DeleteBehavior = DeleteBehavior.Restrict;
            }

/*            modelBuilder.Entity<User>()
                .HasMany(u => u.ContestsJuri)
                .WithMany(c => c.Juri)
                .UsingEntity(c => c.ToTable("ContestJuri"));


            modelBuilder.Entity<User>()
                .HasMany(u => u.ContestsParticipant)
                .WithMany(c => c.Participants)
                .UsingEntity(c => c.ToTable("ContestParticipant"));

            modelBuilder.Entity<Photo>()
                .HasMany(u => u.Contests)
                .WithMany(c => c.Photos)
                .UsingEntity(c => c.ToTable("ContestPhoto"));*/

            modelBuilder.Entity<Trooper>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            modelBuilder.Entity<Monster>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            modelBuilder.Entity<Training>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            modelBuilder.Entity<Post>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
            modelBuilder.Entity<Comment>().HasQueryFilter(x => EF.Property<bool>(x, "IsDeleted") == false);
        }

        protected override void OnConfiguring(DbContextOptionsBuilder options)
        {
            base.OnConfiguring(options);
        }

        public override int SaveChanges()
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChanges();
        }

        public override Task<int> SaveChangesAsync(bool acceptAllChangesOnSuccess, CancellationToken cancellationToken = default)
        {
            UpdateSoftDeleteStatuses();
            return base.SaveChangesAsync(acceptAllChangesOnSuccess, cancellationToken);
        }

        private void UpdateSoftDeleteStatuses()
        {
            foreach (var entry in ChangeTracker.Entries().Where(x => x.Properties.Any(p => p.Metadata.Name == "IsDeleted")))
            {
                switch (entry.State)
                {
                    case EntityState.Added:
                        entry.CurrentValues["IsDeleted"] = false;
                        break;
                    case EntityState.Deleted:
                        entry.State = EntityState.Modified;
                        entry.CurrentValues["IsDeleted"] = true;
                        break;
                }
            }
        }
    }
}
