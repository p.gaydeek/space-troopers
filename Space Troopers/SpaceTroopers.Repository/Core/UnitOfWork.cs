﻿namespace SpaceTroopers.Repository.Core
{
    using System.Threading.Tasks;

    using SpaceTroopers.Repository.Contracts;
    using SpaceTroopers.Repository.Core.IConfiguration;
    using SpaceTroopers.Repository.Core.IRepositories;

    public class UnitOFWork<TEntity> : IUnitOfWork<TEntity> where TEntity : class, IEntity
    {
        private readonly SpaceTroopersContext _context;

        public UnitOFWork(SpaceTroopersContext context)
        {
            _context = context;
            BaseRepository = new BaseRepository<TEntity>(context);
        }

        public IBaseRepository<TEntity> BaseRepository { get; }

        public async Task Commit() => await _context.SaveChangesAsync();

        public void Dispose() => _context.Dispose();
    }
}
