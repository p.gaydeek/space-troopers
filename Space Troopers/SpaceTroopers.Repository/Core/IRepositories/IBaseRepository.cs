﻿namespace SpaceTroopers.Repository.Core.IRepositories
{
    using SpaceTroopers.Repository.Contracts;
    public interface IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        Task<TEntity> Get(int id);
        Task<IEnumerable<TEntity>> GetAll();
        Task Add(TEntity entity);
        void Delete(TEntity entity);
        void Update(TEntity entity);
    }
}
