﻿namespace SpaceTroopers.Repository.Core
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    using SpaceTroopers.Repository.Contracts;
    using SpaceTroopers.Repository.Core.IRepositories;

    public class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity
    {
        DbSet<TEntity> Table { get; set; }

        public BaseRepository(SpaceTroopersContext context)
        {
            Table = context.Set<TEntity>();
        }


        public async Task Add(TEntity entity) => await Table.AddAsync(entity);

        public void Delete(TEntity entity) => Table.Remove(entity);

        public async Task<TEntity> Get(int id) => await Table.SingleAsync(x => x.Id == id);

        public async Task<IEnumerable<TEntity>> GetAll() => await Table.AsNoTracking().ToListAsync();

        public void Update(TEntity entity) => Table.Update(entity);
    }
}
