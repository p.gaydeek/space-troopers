﻿namespace SpaceTroopers.Repository.Core.IConfiguration
{
    using SpaceTroopers.Repository.Contracts;
    using SpaceTroopers.Repository.Core.IRepositories;
    public interface IUnitOfWork<TEntity> : IDisposable where TEntity : class, IEntity
    {
        IBaseRepository<TEntity> BaseRepository { get; }
        Task Commit();
    }
}
