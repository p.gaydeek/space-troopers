﻿namespace SpaceTroopers.Repository.Contracts
{
    using SpaceTroopers.Repository.Models;
    public interface ILikeable
    {
        int TrooperId { get; set; }
        Trooper Trooper { get; set; }
        IList<Like> Likes { get; set; }
    }
}
