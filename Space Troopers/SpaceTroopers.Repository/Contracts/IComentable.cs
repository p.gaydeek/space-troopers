﻿namespace SpaceTroopers.Repository.Contracts
{
    using SpaceTroopers.Repository.Models;
    public interface IComentable
    {
        int TrooperId { get; set; }
        Trooper Trooper { get; set; }
        IList<Comment> Comments { get; set; }
    }
}
