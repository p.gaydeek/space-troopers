﻿namespace SpaceTroopers.Repository.Contracts
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
