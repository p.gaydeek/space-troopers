﻿namespace SpaceTroopers.Repository.Enums
{
    public enum Reaction
    {
        Neutral = 0,
        Like = 1,
        Dislike = -1
    }
}
