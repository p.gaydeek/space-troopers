﻿namespace SpaceTroopers.Domain.Services
{
    using System.Collections.Generic;
    using System.Threading.Tasks;

    using AutoMapper;

    using SpaceTroopers.Domain.Services.Contracts;
    using SpaceTroopers.Repository.Core.IConfiguration;
    using SpaceTroopers.Repository.Models;
    using SpaceTroopers.Repository.Models.Dtos;

    public class TrooperService : ITrooperService
    {
        private readonly IUnitOfWork<Trooper> _unitOfWork;
        private readonly IMapper _mapper;

        public TrooperService(IUnitOfWork<Trooper> unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<IEnumerable<TrooperResponseDto>> GetAllAsync() => _mapper.Map<IEnumerable<TrooperResponseDto>>(await _unitOfWork.BaseRepository.GetAll());

        public async Task<TrooperResponseDto> GetByIdAsync(int id) => _mapper.Map<TrooperResponseDto>(await _unitOfWork.BaseRepository.Get(id));

        public async Task<TrooperResponseDto> GetByUsernameAsync(string username)
        {
            var allTroopers = await GetAllAsync();
            return _mapper.Map<TrooperResponseDto>(allTroopers.Where(tr => tr.UserName == username));
        }
        public async Task DeleteAsync(int id) { _unitOfWork.BaseRepository.Delete(await _unitOfWork.BaseRepository.Get(id)); }
    }
}
