﻿namespace SpaceTroopers.Domain.Services.Contracts
{
    using SpaceTroopers.Repository.Models.Dtos;
    public interface ITrooperService
    {
        Task<IEnumerable<TrooperResponseDto>> GetAllAsync();
        Task<TrooperResponseDto> GetByIdAsync(int id);
        Task<TrooperResponseDto> GetByUsernameAsync(string username);
        Task DeleteAsync(int id);
    }
}
