﻿namespace SpaceTroopers.Domain.MappingProfiles
{
    using AutoMapper;

    using SpaceTroopers.Repository.Models;
    using SpaceTroopers.Repository.Models.Dtos;

    public class DomainToResponseProfile : Profile
    {
        public DomainToResponseProfile()
        {
            CreateMap<Trooper, TrooperResponseDto>();
        }
    }
}
